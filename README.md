# My Paperwork Setup

My setup to organize paperwork at home.

## Prearrangement

All documents are either scanned instantly or organized into drawers or
cardboard slipcases.

## Hardware

Scanner:
* [ScanSnap ix100](https://www.fujitsu.com/us/products/computing/peripheral/scanners/scansnap/ix100/)
  + Small, portable, fast, battery powered, wireless app
* Any phone with a Scanner app (see software below)
  + Always available, very fast
  - Requires additional file sync and post-treatment, not suitable for contracts

NAS
* [Synology DS213](https://www.synology.com/en-us/products/DS218)
  + hassle-free installation & maintenance, powerful apps

Cloud storage server
* Any online storage with a self-hosted cloud software (see software below)
  + Always available, file sharing
  - Requires additional maintenance

## Scanning Software

### Scanner App

[Simple Scan / Document Scanner](https://gitlab.gnome.org/GNOME/simple-scan)

```bash
sudo apt-get install simple-scan
```

On phones I like using the [ScanBot](https://scanbot.io/) App.

### Scanner CLI

[scanimage](https://www.systutorials.com/docs/linux/man/1-scanimage/)

```bash
sudo apt-get install sane-utils
```

```bash
scanimage \
--format=tiff \
--mode lineart \
--resolution 300 \
--page-width 210mm --page-height 297mm \
-p \
-v \
> test.tiff
```

### PDF Conversion

```bash
sudo apt-get install libtiff-tools imagemagick
```

```bash
tiff2pdf test.tiff -o test.pdf
```

```bash
convert test.tiff test.pdf
```

### OCR

```bash
sudo apt-get install ocrmypdf tesseract-ocr-deu
```

```bash
ocrmypdf -l deu test.pdf test.txt.pdf
```

Find all pages in a folder and apply OCR

```bash
find -name "*.pdf" | xargs -i ocrmypdf {} {} -l deu --rotate-pages --title {} --clean --rotate-pages-threshold 5
```

## Document software

### Document organisation

[Tagspaces](https://docs.tagspaces.org/installation.html#linux)

### Document synchronisation

[Synology Cloud Station Drive](https://www.synology.com/en-uk/knowledgebase/DSM/help/CloudStationDrive/cloudstationdrive)

[Nextcloud File Sync](https://nextcloud.com/)

## Misc

* Fake print & sign a document (Bash Script)
  [falsisign](https://gitlab.com/edouardklein/falsisign)

* Add scanner effects like rotation & artifacts to documents (Browser Tool)
  [lookscanned.io](https://lookscanned.io/)

## Custom Scripts

### Scanner Script

Scan, rename, convert to PDF, apply OCR.

```bash
sudo apt-get install sane-utils ocrmypdf tesseract-ocr-deu libtiff-tools
```

```bash
./bin/scan [filename (string)] [qualitylevel (integer 1-4)] [multiple-pages (boolean)]
```

![Demo](./docs/screenshot.gif)

## Scanning best practice

- Universal file formats, such as TIFF or PDF
- Greyscale and small file size for not important documents like supermarket
  bills or informal letters
- Color and large file size for important documents like contracts or
  unique letters
- Naming, naming, naming! Keep a consistent naming structure,
  add dates and context information
- Multiple backups - dont have a single point of failure!
- Keep a copy of very important physical documents, throw away everything else

## Troubleshooting

### Scanner not detected

Use the right USB cable! Not all cables transmit data and
not all cables work with all scanners.

### German umlauts not detected in ocrmypdf

Explicitly set the german language using `-l deu`

https://www.pro-linux.de/artikel/2/1892/4,ocr-einsetzen.html

### Document dimensions too small (no A4)

The order of the options in scanimage matter!

https://unix.stackexchange.com/questions/407403/scanimage-legal-size-settings

### Convert does not work with PDFs

`convert` blocks PDF conversion due to a possible security risk.

It is denied in a policyfile by default, which may be edited.

https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion

### ScanSnap ix100 support

https://jeltsch.org/iX100

```bash
sudo add-apt-repository ppa:rolfbensch/sane-git
```

```bash
scanimage -L
device `fujitsu:ScanSnap iX100:1213359' is a FUJITSU ScanSnap iX100 scanner
```
